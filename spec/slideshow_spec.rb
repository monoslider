require File.dirname(__FILE__) + '/spec_helper'

describe "Creating a slideshow from a single album without specifying prefs" do
  
  before(:each) do
    @controller = Controller.new
    @album = Model.new
    @album.stub!(:slideshow_album_xml)
  end
  
  it "should return a string" do
    @controller.mono_slideshow_xml(@album).should be_an_instance_of String
  end
  
  it "should call slideshow_album_xml on the album once" do
    @album.should_receive(:slideshow_album_xml).once
    @controller.mono_slideshow_xml(@album)
  end
  
  it "should return expected XML doc with defaults" do
    @album.should_receive(:slideshow_album_xml).once
    slideshow = @controller.mono_slideshow_xml(@album)
    slideshow.should be_an_instance_of String
    pref_hash = {"imageScaleMode"=>"scaleToFit", "showAlbumsButton" => "false"}
    Hash.from_xml(slideshow).should == {"slideshow"=>{"preferences"=> pref_hash}}
  end
  
  it "should convert each album to xml" do
    @album.should_receive(:slideshow_album_xml).once
    @controller.mono_slideshow_xml(@album)
  end
  
end

describe "Creating a slideshow from multiple albums" do
  
  before(:each) do
    @controller = Controller.new
    @album = Model.new
    @album.stub!(:slideshow_album_xml)
    @albums = [@album,@album]
  end

  
  it "should return an xml doc" do
    @album.should_receive(:slideshow_album_xml).twice
    slideshow = @controller.mono_slideshow_xml(@albums)
    slideshow.should be_an_instance_of String
    pref_hash = {"imageScaleMode"=>"scaleToFit"}
    Hash.from_xml(slideshow).should == {"slideshow"=>{"preferences"=> pref_hash}}
  end
  
  it "should convert each album to xml" do
    @album.should_receive(:slideshow_album_xml).exactly(@albums.size).times
    @controller.mono_slideshow_xml(@albums)
  end
  
end

describe "Specifying preferences for a slideshow" do
  it "should return an XML doc with global prefs" do
    @controller = Controller.new
    @album = Model.new
    @album.stub!(:slideshow_album_xml)
    @albums = [@album,@album]
    slideshow = @controller.mono_slideshow_xml(@albums, :showThumbnailsButton => :false, :imageScaleMode => :scaleToFill)
    slideshow.should be_an_instance_of String
    pref_hash = {"imageScaleMode"=>"scaleToFill", "showThumbnailsButton" => "false"}
    Hash.from_xml(slideshow).should == {"slideshow"=>{"preferences"=> pref_hash}}
  end
end
