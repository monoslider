ActiveRecord::Schema.define(:version => 0) do
  create_table :albums, :force => true do |t|
    t.column :name, :string
  end
  
  create_table :images, :force => true do |t|
    t.column "parent_id",    :integer
    t.column "content_type", :string
    t.column "filename",     :string
    t.column "thumbnail",    :string
    t.column "size",         :integer
    t.column "width",        :integer
    t.column "height",       :integer
  end
end
