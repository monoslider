require File.dirname(__FILE__) + '/spec_helper'

describe "Aliased Array#=== method" do
  before(:each) do
    @a1 = [:thing_one, :thing_two]
    @a2 = ['thing_one'.to_sym, 'thing_two'.to_sym]
    @a3 = [1, 2]
  end
  
  it "should be callable as #case_equal?" do
    @a1.case_equal?(@a2).should be_true
    @a1.case_equal?(@a3).should be_false
  end

end

describe "Redefined Array#=== method on flat arrays" do
  it "should return true when Array#== is true, even if elements are not case equal" do
    [String, String, Fixnum].===([String, String, Fixnum]).should be_true
  end
  
  it "should return true when all elements are case equal" do
    [Symbol].===([:one]).should == Symbol.===(:one)
    [/one/].===(['one']).should == /one/.===('one')
    [(0..2)].===([1]).should == (0..2).===(1)
  end
end

describe "Redefined Array#=== on nested arrays" do
  before(:each) do
    @other = [1, 'one', [:one, :two, [], [1.0]]]
    @matcher = [(0..3), String, [:one, Symbol, [], [Float]]]
    @matcher2 = [[]]
    @other2 = [[:one]]
  end
  it "should return true when all elements and subelements are case equal" do
    @matcher.===(@other).should be_true
  end
  
  it "should return false when all elements are not case equal" do
    @matcher.===(@other2).should be_false
  end
end