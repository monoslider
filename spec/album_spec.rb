require File.dirname(__FILE__) + '/spec_helper'

def minimal_slideshow1
  Model.slideshow_album :from => :pictures, :slides => { :src => :absolute_path }
end

def minimal_slideshow2
  # this form should result in minimal slideshow album attributes
  Model.slideshow_album :pictures, :public_filename
end

def customized_slideshow1
  Model.slideshow_album :from => :pictures, :title => :display_name, :description => :notes,
    :slides => { :src => :public_filename, :title => :name, :description => :caption }
end

# describe "Calling the class method slideshow_album(:from => :images)" do
#    
#   it "should define #slideshow_album_xml as an instance method" do
#     Model.slideshow_album :from => :images
#     Model.new.should respond_to :slideshow_album_xml
#   end
#   
# end

describe "Calling slideshow_album without arguments" do
  it "should raise an error" do
    lambda {Model.slideshow_album}.should raise_error ArgumentError
  end
end

describe "Calling slideshow_album with simple arg form" do
  before(:each) do
    Model.slideshow_album :pictures, :public_filename
    @album = Model.new
  end
  
  it "should define Model#slideshow_album_xml" do
    @album.should respond_to :slideshow_album_xml
  end
  
  it "should define hash and accessor for the slideshow config" do
    @album.mono_album_options[:from].should == :pictures
  end
  
  it "should create a method for accessing the slide collection" do
    @album.should_receive(:pictures)
    @album.mono_album_slides
  end
  
  it "should create a method for accessing slide src" do
    @slide = mock('slide')
    @slide.should_receive(:public_filename)
    @album.mono_slide_src(@slide)
  end
  
  it "should create an album thumbnail src accessor, using the first slide by default" do
    @slide1 = mock('slide')
    @slide2 = mock('slide')
    @slides = [@slide1, @slide2]
    @slide1.should_receive(:public_filename).and_return('file.jpg')
    @album.should_receive(:mono_album_slides).and_return(@slides)
    @album.mono_album_thumbnail.should == 'file.jpg'
  end
    
end

describe "Calling slideshow_album, full form, minimal args" do
  before(:each) do
    Model.slideshow_album :from => :pictures, :slides => { :src => :public_filename }
    @album = Model.new
  end
  
  it "should define Model#slideshow_album_xml" do
    @album.should respond_to :slideshow_album_xml
  end
  
  it "should define hash and accessor for the slideshow config" do
    @album.mono_album_options[:from].should == :pictures
  end
  
  it "should create a method for accessing the slide collection" do
    @album.should_receive(:pictures)
    @album.mono_album_slides
  end
  
  it "should create a method for accessing slide src" do
    @slide = mock('slide')
    @slide.should_receive(:public_filename)
    @album.mono_slide_src(@slide)
  end
  
  it "should create an album thumbnail src accessor, using the first slide by default" do
    @slide1 = mock('slide')
    @slide2 = mock('slide')
    @slides = [@slide1, @slide2]
    @slide1.should_receive(:public_filename).and_return('file.jpg')
    @album.should_receive(:mono_album_slides).and_return(@slides)
    @album.mono_album_thumbnail.should == 'file.jpg'
  end
    
end

describe "The slideshow_album_xml method, when created with minimal args" do
  before(:each) do
    Model.slideshow_album :pictures, :public_filename
    @album = Model.new
    @picture = mock 'picture'
    @pictures = [@picture, @picture]
    @album.should_receive(:pictures).twice.and_return(@pictures)
    @picture.should_receive(:public_filename).exactly(@pictures.size + 1).times.and_return('file.jpg')
    @album_xml = @album.slideshow_album_xml
  end
  it "should return a properly constructed xml doc" do
    @album_xml.should be_an_instance_of String
    Hash.from_xml(@album_xml).should == { "album" => {
        "thumbnail" => "file.jpg",
        "img"=> [{"src" => "file.jpg"}, {"src" => "file.jpg"}] } }
  end
end

describe "Calling slideshow_album with title and description args" do
  before(:each) do
    Model.slideshow_album :from => :pictures, :title => :display_name, :description => :notes,
      :slides => { :src => :public_filename, :title => :name, :description => :caption }
    @album = Model.new
  end
  
  it "should define Model#slideshow_album_xml" do
    @album.should respond_to :slideshow_album_xml
  end
  
  it "should define hash and accessor for the slideshow config" do
    @album.mono_album_options[:from].should == :pictures
    @album.mono_album_options[:title].should == :display_name
    @album.mono_album_options[:description].should == :notes
  end
  
  it "should set the album collection method" do
    @album.should_receive(:pictures)
    @album.mono_album_slides
  end
  
  it "should set the album title method" do
    @album.should_receive(:display_name)
    @album.mono_album_title
  end
  
  it "should set the album description method" do
    @album.should_receive(:notes)
    @album.mono_album_description
  end
  
  it "should set the slide options" do
    @album.mono_slide_options[:src].should == :public_filename
    @album.mono_slide_options[:title].should == :name
    @album.mono_slide_options[:description].should == :caption
  end
  
  it "should create a method for accessing slide src" do
    @slide = mock('slide')
    @slide.should_receive(:public_filename)
    @album.mono_slide_src(@slide)
  end
  
  it "should create a method for accessing slide titles" do
    @slide = mock('slide')
    @slide.should_receive(:name)
    @album.mono_slide_title(@slide)
  end
  
  it "should create a method for accessing slide descriptions" do
    @slide = mock('slide')
    @slide.should_receive(:caption)
    @album.mono_slide_description(@slide)
  end
  
  it "should create an album thumbnail src accessor, using the first slide by default" do
    @slide1 = mock('slide')
    @slide2 = mock('slide')
    @slides = [@slide1, @slide2]
    @slide1.should_receive(:public_filename).and_return('file.jpg')
    @album.should_receive(:mono_album_slides).and_return(@slides)
    @album.mono_album_thumbnail.should == 'file.jpg'
  end
  
end

describe "The slideshow_album_xml method, when created with title and description" do
  before(:each) do
    Model.slideshow_album :from => :pictures, :title => :display_name, :description => :notes,
      :slides => { :src => :public_filename, :title => :name, :description => :caption }
    @album = Model.new
    @picture = mock 'picture'
    @pictures = [@picture, @picture]
    @album.should_receive(:pictures).twice.and_return(@pictures)
    @album.should_receive(:display_name).and_return('Some Photos I took')
    @album.should_receive(:notes).and_return('All of these are photos that I took.')
    @picture.should_receive(:public_filename).exactly(@pictures.size + 1).times.and_return('file.jpg')
    @picture.should_receive(:name).exactly(@pictures.size).times.and_return('A Photo')
    @picture.should_receive(:caption).exactly(@pictures.size).times.and_return('I took it')
    @album_xml = @album.slideshow_album_xml
  end
  
  it "should return an xml doc with titles and descriptions for albums and slides" do
     Hash.from_xml(@album_xml).should == {"album" => {
        "title" => "Some Photos I took", 
        "description" => "All of these are photos that I took.", 
        "thumbnail" => "file.jpg",
        "img"=>[
          {"title" => "A Photo", "src" => "file.jpg", "description" => "I took it"}, 
          {"title" => "A Photo", "src" => "file.jpg", "description" => "I took it"}]}}
  end
end


