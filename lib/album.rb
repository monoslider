module MonoSlideshow
  module Album
    def self.included(base)
         base.extend ClassMethods
       end
      
    module ClassMethods
      def slideshow_album(*args)
        @@mono_album_options = {}
        @@mono_slide_options = {}
        define_method(:mono_album_options) { @@mono_album_options }
        define_method(:mono_slide_options) { @@mono_slide_options }
        case args
        when [Symbol, Symbol]
          @@mono_album_options[:from] = args[0]
          @@mono_slide_options[:src] = args[1]
        when [Hash]
          options = args[0]
          @@mono_slide_options = options.delete(:slides)
          @@mono_album_options = options
        else
          raise ArgumentError
        end
        
        define_method(:mono_album_slides) {self.send(mono_album_options[:from])}
        
        define_method(:mono_album_title) do
          self.send(mono_album_options[:title]) if mono_album_options[:title]
        end
        
        define_method(:mono_album_description) do
          self.send(mono_album_options[:description]) if mono_album_options[:description]
        end
        
        define_method(:mono_slide_src) { |slide| slide.send mono_slide_options[:src] }
        
        define_method(:mono_slide_title) do |slide|
          slide.send mono_slide_options[:title] if mono_slide_options[:title]
        end
        
        define_method(:mono_slide_description) do |slide|
          slide.send mono_slide_options[:description] if mono_slide_options[:description]
        end

        define_method(:mono_album_thumbnail) { mono_slide_src(mono_album_slides.first) }
        
        include InstanceMethods
        
    #     default_options = { :from         => :images,
    #                         :title        => :title,
    #                         :description  => :description,
    #                         :slide        => {  :title => :title, 
    #                                             :src => :public_filename, 
    #                                             :description => :description }}
    #                         
    #     options = default_options.merge(options)
    #     @@mono_slide_association = options[:from]
    #     @@mono_slide_src_method = options[:slide][:src]
    #     @@mono_slide_description_method = options[:slide][:description]
    #     @@mono_album_title = options[:title]
    #     @@mono_album_description = options[:description]
    #   
    #     define_method(:mono_slide_src_method) { @@mono_slide_src_method }
    #     define_method(:mono_slide_description_method) { @@mono_slide_description_method }
    #     define_method(:mono_album_title) { self.send(@@mono_album_title) }  
    #     define_method(:mono_album_description) { self.send(@@mono_album_description) }
    # 
      end
    end
      
    module InstanceMethods

      def slideshow_album_xml(xml=nil)
        xml ||= Builder::XmlMarkup.new(:indent => 4)
        album_hash = {:title => mono_album_title, 
                      :description => mono_album_description, 
                      :thumbnail => mono_album_thumbnail}.reject { |k,v| v.blank? }
        xml.album(album_hash) do
          mono_album_slides.each do |image|
            image_hash = {:src => mono_slide_src(image), 
                          :title => mono_slide_title(image), 
                          :description => mono_slide_description(image)}.reject { |k,v| v.blank? }
            xml.img(image_hash)
          end
        end
      end    
    end
    
  end
end