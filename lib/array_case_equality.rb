class Array
  
  alias_method :case_equal?, :===
  
  def ===(other, counter = [0], my_rec = {}, his_rec = {}) # !> method redefined; discarding old ===
    return true if other.object_id == self.object_id
    return false unless other.is_a? Array
    return true if other == self
    return false unless  other.size == self.size
    
    counter[0] += 1
    my_rec[object_id] = his_rec[other.object_id] = counter[0]
    
    size.times do |i|
      x, y = self[i], other[i]
      xid, yid = x.object_id, y.object_id
      if x.is_a? Array and y.is_a? Array
        recursive = my_rec[xid] || his_rec[yid]
        if recursive && my_rec[xid] != his_rec[yid]
          return false
        elsif recursive # matches because the two arrays have same object_id
          next
        else # not recursive, just compare normally
          return false unless x.===(y, counter, my_rec, his_rec)
        end
      elsif x.is_a? Array
        # if x is an Array, but y is not
        return false
      else
        # general case
        return false unless x === y
      end
    end
    
    true
  end
end
