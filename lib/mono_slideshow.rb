require 'album'
require 'array_case_equality'


module MonoSlideshow
  module SlideshowXML
    
    DEFAULTS = { :imageScaleMode => :scaleToFit }
    
    def self.included(base)
      
    end
    
    def mono_slideshow_xml(albums, preferences={})
      albums = [albums].flatten
      merged_prefs = albums.size == 1 ? {:showAlbumsButton => 'false'} : {}
      merged_prefs.merge!(preferences).reverse_merge!(DEFAULTS)
      xml = Builder::XmlMarkup.new
      xml.instruct! :xml, :version => '1.0', :encoding => 'utf-8'
      xml.slideshow do
        xml.preferences(merged_prefs)
        albums.each do |album|
          album.slideshow_album_xml(xml)
        end
      end 
      
    end
  end
  
end

class Hash
    def recursive_merge(h)
        self.merge(h) {|key, _old, _new| if _old.class == Hash then _old.recursive_merge(_new) else _new end  } 
    end
    
    def recursive_merge!(h)
        self.merge!(h) {|key, _old, _new| if _old.class == Hash then _old.recursive_merge!(_new) else _new end  } 
    end
end