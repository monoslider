require 'mono_slideshow'
require 'album'
require 'slideshow'
ActiveRecord::Base.send(:include, MonoSlideshow::Album)
ActionController::Base.send(:include, MonoSlideshow::SlideshowXML)